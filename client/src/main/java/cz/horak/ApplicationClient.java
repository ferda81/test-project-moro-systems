package cz.horak;

import cz.horak.handler.StompHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;

@SpringBootApplication
public class ApplicationClient implements CommandLineRunner {

	public static void main(String[] args) throws InterruptedException {
		SpringApplication.run(ApplicationClient.class, args);
		while(true){
			Thread.sleep(1000);
		}
	}

	@Override
	public void run(String... args) throws Exception {

	}
}
