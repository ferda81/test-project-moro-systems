package cz.horak.handler;

import cz.horak.dto.DiskDataDTO;
import cz.horak.dto.MemoryDataDTO;
import cz.horak.dto.MessageDTO;
import cz.horak.dto.ThreadDataDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.messaging.simp.stomp.StompSessionHandler;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.client.WebSocketClient;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import org.springframework.web.socket.messaging.WebSocketStompClient;

import java.io.File;
import java.io.IOException;
import java.lang.management.*;
import java.lang.reflect.Type;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class StompHandler implements StompSessionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(StompHandler.class);

    private StompSession stompSession;

    private StompHeaders connectedHeaders;


    @EventListener(value = ApplicationReadyEvent.class)
    public void connect() {
        LOGGER.info("connecting to STOMP.");
        WebSocketClient client = new StandardWebSocketClient();
        WebSocketStompClient stompClient = new WebSocketStompClient(client);
        stompClient.setMessageConverter(new MappingJackson2MessageConverter());
        try {
            stompSession = stompClient
                    .connectAsync("ws://localhost:8080/messagesInput", this)
                    .get();
            LOGGER.info("config done.");
        } catch (Exception e) {
            System.err.println("Connection failed." + e.getMessage());
        }
    }
    @Override
    public void afterConnected(StompSession session, StompHeaders connectedHeaders) {
        LOGGER.info("connected.");
        LOGGER.info("New session established : " + session.getSessionId());
        session.subscribe("/topic/messages", this);
        LOGGER.info("Subscribed to /topic/messages");
        this.connectedHeaders = connectedHeaders;
        while(true){
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            MessageDTO mess = getPeriodicMessage(session.getSessionId());
            session.send("/app/messagesInput", mess);
        }



    }

    public MessageDTO getPeriodicMessage(String sessionId) {
        ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
        MessageDTO message = new MessageDTO();

        MemoryMXBean memoryManagerMXBean = ManagementFactory.getMemoryMXBean();
        MemoryDataDTO memoryData = new MemoryDataDTO();
        memoryData.setCommited(memoryManagerMXBean.getHeapMemoryUsage().getCommitted());
        memoryData.setInit(memoryManagerMXBean.getHeapMemoryUsage().getInit());
        memoryData.setUsed(memoryManagerMXBean.getHeapMemoryUsage().getUsed());

        File cDrive = new File("C:");
        DiskDataDTO diskData = new DiskDataDTO();

        diskData.setTotalSpace(cDrive.getTotalSpace());
        diskData.setFreeSpace(cDrive.getFreeSpace());
        diskData.setUseableSpace(cDrive.getUsableSpace());

        List<ThreadDataDTO> threadDatas = new ArrayList<>();
        for(Long threadID : threadMXBean.getAllThreadIds()) {
            ThreadInfo info = threadMXBean.getThreadInfo(threadID);
            ThreadDataDTO tdata = new ThreadDataDTO();
            tdata.setName(info.getThreadName());
            tdata.setState(info.getThreadState().name());
            tdata.setCpuTime(threadMXBean.getThreadCpuTime(threadID));
            threadDatas.add(tdata);
        }

        message.setThreadInfos(threadDatas);
        message.setClientUUId(sessionId);
        message.setDate(new Date());
        message.setDiskData(diskData);
        message.setMemoryData(memoryData);

        return message;
    }

    @Override
    public void handleException(StompSession session, StompCommand command, StompHeaders headers, byte[] payload, Throwable exception) {
        LOGGER.error("", exception);
    }

    @Override
    public void handleTransportError(StompSession session, Throwable exception) {
        LOGGER.error("", exception);
    }

    @Override
    public Type getPayloadType(StompHeaders headers) {
        return null;
    }

    @Override
    public void handleFrame(StompHeaders headers, Object payload) {
        LOGGER.info(payload.toString());
    }
}
