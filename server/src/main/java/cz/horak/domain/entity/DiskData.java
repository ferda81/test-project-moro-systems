package cz.horak.domain.entity;

import jakarta.persistence.*;

@Entity
@Table(name = "DISK_DATA")
public class DiskData {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name = "message_id")
    private Message message;

    @Column(name="TOTAL_SPACE")
    private long totalSpace;

    @Column(name="FREE_SPACE")
    private long freeSpace;

    @Column(name="USEABLE_SPACE")
    private long useableSpace;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(long totalSpace) {
        this.totalSpace = totalSpace;
    }

    public long getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(long freeSpace) {
        this.freeSpace = freeSpace;
    }

    public long getUseableSpace() {
        return useableSpace;
    }

    public void setUseableSpace(long useableSpace) {
        this.useableSpace = useableSpace;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
