package cz.horak.domain.entity;

import jakarta.persistence.*;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "MESSAGE")
public class Message {

    @Id
    @GeneratedValue
    private Long id;

    @Column( name = "CLIENT_UUID")
    private String clientUUId;

    @Column(name = "DATE")
    private Date date;

    @OneToOne(mappedBy = "message")
    private DiskData diskData;

    @OneToOne(mappedBy = "message")
    private MemoryData memoryData;

    @OneToMany(mappedBy = "message")
    private List<ThreadData> threadDatas;

    public DiskData getDiskData() {
        return diskData;
    }

    public void setDiskData(DiskData diskData) {
        this.diskData = diskData;
    }

    public MemoryData getMemoryData() {
        return memoryData;
    }

    public void setMemoryData(MemoryData memoryData) {
        this.memoryData = memoryData;
    }

    public List<ThreadData> getThreadDatas() {
        return threadDatas;
    }

    public void setThreadDatas(List<ThreadData> threadDatas) {
        this.threadDatas = threadDatas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getClientUUId() {
        return clientUUId;
    }

    public void setClientUUId(String clientUUId) {
        this.clientUUId = clientUUId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
