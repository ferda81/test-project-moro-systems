package cz.horak.domain.dao;

import cz.horak.domain.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repository bean for CRUD operations.
 */
public interface MessageDao extends JpaRepository<Message, Long> {

}
