package cz.horak.domain.dao;

import cz.horak.domain.entity.Message;
import cz.horak.domain.entity.ThreadData;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository bean for CRUD operations.
 */
public interface ThreadDataDao extends JpaRepository<ThreadData, Long> {

}
