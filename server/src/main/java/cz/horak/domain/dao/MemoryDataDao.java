package cz.horak.domain.dao;

import cz.horak.domain.entity.MemoryData;
import cz.horak.domain.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository bean for CRUD operations.
 */
public interface MemoryDataDao extends JpaRepository<MemoryData, Long> {

}
