package cz.horak.domain.dao;

import cz.horak.domain.entity.DiskData;
import cz.horak.domain.entity.Message;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repository bean for CRUD operations.
 */
public interface DiskDataDao extends JpaRepository<DiskData, Long> {

}
