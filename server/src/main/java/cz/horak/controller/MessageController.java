package cz.horak.controller;

import cz.horak.dto.MessageDTO;
import cz.horak.rest.api.MessagesApi;
import cz.horak.rest.model.*;
import cz.horak.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Rest controller for returning saved data in DB.
 */
@Controller
public class MessageController implements MessagesApi {

    @Autowired
    private MessageService messageService;

    @Override
    public ResponseEntity<Result> getMessages(Integer lastMinutes) {
        return new ResponseEntity<>(getResult(lastMinutes), HttpStatus.OK);
    }

    private Result getResult(Integer lastMiutes){

        List<MessageDTO> data = messageService.getMessages();
        //filtring by minutes, add to DB layer - better solution.

        Calendar now = Calendar.getInstance();
        int minutes = now.get(Calendar.MINUTE);
        now.set(Calendar.MINUTE,minutes - lastMiutes);
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

        data = data.stream().filter(item -> item.getDate().compareTo(now.getTime()) > 0).collect(Collectors.toList());

        List<MessageTO> dataToReturn = data.stream().map( item ->{
            MessageTO to = new MessageTO();
            to.setClientUUId(item.getClientUUId());
            to.setDate(dateFormat.format(item.getDate()));

            if(item.getDiskData() != null) {
                DiskDataTO diskDataTO = new DiskDataTO();
                diskDataTO.setFreeSpace(item.getDiskData().getFreeSpace());
                diskDataTO.setTotalSpace(item.getDiskData().getTotalSpace());
                diskDataTO.setUseableSpace(item.getDiskData().getUseableSpace());
                to.setDiskData(diskDataTO);
            }

            if(item.getMemoryData() != null) {
                MemoryDataTO memoryDataTO = new MemoryDataTO();
                memoryDataTO.setCommited(item.getMemoryData().getCommited());
                memoryDataTO.setInit(item.getMemoryData().getInit());
                memoryDataTO.setUsed(item.getMemoryData().getUsed());
                to.setMemoryData(memoryDataTO);
            }

            if(item.getThreadInfos() != null) {
                List<ThreadDataTO> threadTos = item.getThreadInfos().stream().map(itemInf -> {
                    ThreadDataTO threadDataTO = new ThreadDataTO();
                    threadDataTO.setName(itemInf.getName());
                    threadDataTO.setState(itemInf.getState());
                    threadDataTO.setCpuTime(itemInf.getCpuTime());
                    return threadDataTO;
                }).toList();
                to.setThreadData(threadTos);
            }
            return to;
        }).collect(Collectors.toList());
        return new Result().items(dataToReturn).total(dataToReturn.size());
    }
}
