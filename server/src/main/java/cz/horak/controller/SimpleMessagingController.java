package cz.horak.controller;

import cz.horak.dto.MessageDTO;
import cz.horak.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;

/**
 *  Controller for processing messages camming from Websockets (STOMP)
 */
@Controller
public class SimpleMessagingController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimpleMessagingController.class);

    @Autowired
    private MessageService messageService;

    @MessageMapping("/messagesInput")
    public void readMessage(MessageDTO message) {
        LOGGER.info("New incoming message: "+ message.toString());
        messageService.saveMessage(message);
        LOGGER.info("Message processed.");
    }
}

