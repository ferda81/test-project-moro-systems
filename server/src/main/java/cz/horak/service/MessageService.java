package cz.horak.service;

import cz.horak.dto.MessageDTO;

import java.util.List;
/**
 * Service for serving all relevatns actions to messages
 */
public interface MessageService {

    /**
     * Saves message into DB.
     * @param form
     */
    void saveMessage(MessageDTO form);

    /**
     * returns all available messages in db.
     * @return
     */
    List<MessageDTO> getMessages();

}
