package cz.horak.service;

import cz.horak.domain.dao.DiskDataDao;
import cz.horak.domain.dao.MemoryDataDao;
import cz.horak.domain.dao.MessageDao;
import cz.horak.domain.dao.ThreadDataDao;
import cz.horak.domain.entity.DiskData;
import cz.horak.domain.entity.MemoryData;
import cz.horak.domain.entity.Message;
import cz.horak.domain.entity.ThreadData;
import cz.horak.dto.DiskDataDTO;
import cz.horak.dto.MemoryDataDTO;
import cz.horak.dto.MessageDTO;
import cz.horak.dto.ThreadDataDTO;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class MessageServiceBean implements MessageService{

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private DiskDataDao diskDataDao;

    @Autowired
    private MemoryDataDao memoryDataDao;

    @Autowired
    private ThreadDataDao threadDataDao;

    @Override
    @Transactional
    public void saveMessage(MessageDTO form) {

        Message message = new Message();
        message.setClientUUId(form.getClientUUId());
        message.setDate(form.getDate());
        messageDao.save(message);


        DiskData diskData = new DiskData();
        diskData.setFreeSpace(form.getDiskData().getFreeSpace());
        diskData.setTotalSpace(form.getDiskData().getTotalSpace());
        diskData.setUseableSpace(form.getDiskData().getUseableSpace());
        diskData.setMessage(message);
        diskDataDao.save(diskData);

        MemoryData memoryData = new MemoryData();
        memoryData.setMessage(message);
        memoryData.setUsed(form.getMemoryData().getUsed());
        memoryData.setCommited(form.getMemoryData().getCommited());
        memoryData.setInit(form.getMemoryData().getInit());
        memoryDataDao.save(memoryData);

        form.getThreadInfos().stream().forEach(item ->{
            ThreadData threadData = new ThreadData();
            threadData.setName(item.getName());
            threadData.setCpuTime(item.getCpuTime());
            threadData.setState(item.getState());
            threadData.setMessage(message);
            threadDataDao.save(threadData);
        });

    }

    @Override
    @Transactional
    public List<MessageDTO> getMessages() {
        List<Message> messages = messageDao.findAll();
        return transformToDTO(messages);
    }

    private List<MessageDTO> transformToDTO(List<Message> messages){
        List<MessageDTO> resultList = new ArrayList<>();

        messages.stream().forEach(item -> {
            MessageDTO dto = new MessageDTO();

            dto.setDate(item.getDate());
            dto.setClientUUId(item.getClientUUId());

            DiskData diskData = item.getDiskData();
            DiskDataDTO diskDTO = new DiskDataDTO();
            diskDTO.setFreeSpace(diskData.getFreeSpace());
            diskDTO.setTotalSpace(diskData.getTotalSpace());
            diskDTO.setUseableSpace(diskData.getUseableSpace());

            dto.setDiskData(diskDTO);

            MemoryData memoryData = new MemoryData();
            MemoryDataDTO memoryDTO = new MemoryDataDTO();
            memoryDTO.setCommited(memoryData.getCommited());
            memoryDTO.setInit(memoryData.getInit());
            memoryDTO.setUsed(memoryData.getUsed());
            dto.setMemoryData(memoryDTO);

            dto.setThreadInfos(item.getThreadDatas().stream().map( itemSub -> {
                ThreadDataDTO thdto = new ThreadDataDTO();
                thdto.setName(itemSub.getName());
                thdto.setState(itemSub.getState());
                thdto.setCpuTime(itemSub.getCpuTime());
                return thdto;
            }).collect(Collectors.toList()));

            resultList.add(dto);
        });
        return resultList;
    }
}
