package cz.horak.dto;

public class DiskDataDTO {

    private long totalSpace;

    private long freeSpace;

    private long useableSpace;

    public long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(long totalSpace) {
        this.totalSpace = totalSpace;
    }

    public long getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(long freeSpace) {
        this.freeSpace = freeSpace;
    }

    public long getUseableSpace() {
        return useableSpace;
    }

    public void setUseableSpace(long useableSpace) {
        this.useableSpace = useableSpace;
    }
}
