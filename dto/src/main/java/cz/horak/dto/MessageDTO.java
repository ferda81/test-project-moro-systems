package cz.horak.dto;

import java.util.Date;
import java.util.List;

public class MessageDTO {

    private String clientUUId;

    private Date date;

    private MemoryDataDTO memoryData;

    private DiskDataDTO diskData;

    private List<ThreadDataDTO> threadInfos;

    public MemoryDataDTO getMemoryData() {
        return memoryData;
    }

    public void setMemoryData(MemoryDataDTO memoryData) {
        this.memoryData = memoryData;
    }

    public DiskDataDTO getDiskData() {
        return diskData;
    }

    public void setDiskData(DiskDataDTO diskData) {
        this.diskData = diskData;
    }

    public List<ThreadDataDTO> getThreadInfos() {
        return threadInfos;
    }

    public void setThreadInfos(List<ThreadDataDTO> threadInfos) {
        this.threadInfos = threadInfos;
    }

    public String getClientUUId() {
        return clientUUId;
    }

    public void setClientUUId(String clientUUId) {
        this.clientUUId = clientUUId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "MessageDTO{" +
                "clientUUId='" + clientUUId + '\'' +
                ", date=" + date +
                ", memoryData=" + memoryData +
                ", diskData=" + diskData +
                ", threadInfos=" + threadInfos +
                '}';
    }
}
