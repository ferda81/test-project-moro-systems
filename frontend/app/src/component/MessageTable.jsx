import React, { useState, useEffect } from "react";
import axios from 'axios';


class MessageTable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          items: []
        };
      }

      componentDidMount() {
        fetch("http://localhost:8080/messages")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                items: result.items
              });
            },
            // Note: it's important to handle errors here
              // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
      }  

    fetchData = () => {
      fetch("http://localhost:8080/messages")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                items: result.items
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    } 

	
	render() {
		const {items} = this.state;
		console.log(items);
		return (
			<div>
				<button onClick={this.fetchData}>
					Load
				</button>
			
			<table className="dataTable">
			<thead>
			  
				
				<th align="right">Date</th>
        <th align="right">Client</th>
				<th align="right">Memory (in bytes)</th>
				<th align="right">Disk (in bytes)</th>
        <th align="right">Threads (ms)</th>
			</thead>
			<tbody>
			  {items && items.map((row) => 
				<tr key="{row.clientUUId}-{row.date}">
				  <td >
					  {row.date}
				  </td>
				  <td align="right">{row.clientUUId}</td>
				  <td align="right">{row.diskData.freeSpace} / {row.diskData.freeSpace} / {row.diskData.freeSpace}</td>
				  <td align="right">{row.memoryData.commited} / {row.memoryData.init} / {row.memoryData.used}</td>
				  <td align="right">{row.threadData[0].name} - {row.threadData[0].cpuTime}</td>
				</tr>
			  )}
			</tbody>
		  </table>
		  </div>
	  );
	}
};

export default MessageTable;