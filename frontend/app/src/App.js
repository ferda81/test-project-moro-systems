import MessageTable from './component/MessageTable';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>Tabulka dat</h1>
        <p>
          Tlačítko "Load" umožňuje nové načtení dat ze serveru. 
        </p>
        <MessageTable></MessageTable>
      </header>
      
    </div>
  );
}

export default App;
